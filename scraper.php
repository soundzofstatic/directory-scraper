<?php
try {

    include_once(__DIR__ . '/vendor/autoload.php');

    $text = file_get_contents(__DIR__ . '/data/table-directory.html'); // todo - grab path from command line argument

    // The $text fed to the preg_match_all functions expects for it to be UGLIFIED

    preg_match_all('/<tr(| class="alt")>(.*?)<\/tr>/', $text, $tableRows);

    $file = fopen('data/data.csv', 'w+');

    // Headers
    fwrite($file,
        '"Business Name","Street","City","State","Zip","Phone","Category","Link"' . PHP_EOL
    );

    foreach($tableRows[2] as $row) {

        preg_match_all('/<td style="font-size:small;">(\s|)<span style="font-weight: bold;">(.*)<\/span><br>(.*)<br>(.*)<br><a href\=\"(.*)\" onkeypress="this\.onclick\(\);"(.*)>View Map<\/a><br>Phone: (.*) (|<br>Link:(.*))<br><\/td><td style="font-size:small;">(.*)<\/td>/', trim($row), $fields);

        if(empty($fields[2][0])) {

            continue;

        }

        $link = '';

        if(!empty($fields[8][0])) {

            preg_match_all('/<a href="(.*?)"/', $fields[8][0], $links);

            $link = $links[1][0];

        }

        $cityStateZipCombo = htmlspecialchars_decode(str_replace('&nbsp;', ' ', $fields[4][0])); // State & Zip

        $cityStateZipCombo = str_replace(',', '', $cityStateZipCombo);

        $cityStateZipArray = explode(' ', trim($cityStateZipCombo));

        fwrite($file, '"' . implode('","', [
                    htmlspecialchars_decode($fields[2][0]), // Business Name
                    htmlspecialchars_decode($fields[3][0]), // Street Route
                    $cityStateZipArray[0], // City
                    $cityStateZipArray[1], // State
                    $cityStateZipArray[2], // Zip
//                    $fields[5][0], // Mapquest Link
                    htmlspecialchars_decode($fields[7][0]), // Phone Number
                    htmlspecialchars_decode(str_replace('&nbsp;', ' ', $fields[10][0])), // Category Name
                    $link // Link if it exists
                ]
            ) . '"' . PHP_EOL
        );

    }

    fclose($file);

} catch  (\Exception $e) {

    print_r($e);

}